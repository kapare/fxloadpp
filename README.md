# Cypress FX2 

## Description

This project is fork of the original [fxload](http://www.triplespark.net/elec/periph/USB-FX2/eeprom/fxload-full-2008_10_13-ww3.tar.gz).

The main goal is to to port for c++17. 

## EEPROM Configuration

As shown in the [schematic above](http://www.triplespark.net/elec/periph/USB-FX2/), you can attach an EEPROM (e.g. 24LC64) to the FX2 in order to permanently store the firmware for the 8051.

This way, you do not have to download the firmware each time the FX2 is plugged into the USB.

Instead, it starts up with the firmware and can configure the endpoint descriptors. 

# References

* [Electronics -- USB-FX2 Interface Board (USB-2.0)](http://www.triplespark.net/elec/periph/USB-FX2/)
